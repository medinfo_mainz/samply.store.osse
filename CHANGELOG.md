# Changelog Samply Store OSSE

## Version 1.3.0

- added support for patient user accounts
- added support for special key filters in the `ResourceQueryBuilder`

## Version 1.2.0

- added the `wipeoutResource` method for patients and cases
- added the system role
- added the `ResourceQueryBuilder` that converts a View from the query language
    into a ResourceQuery (experimental)


## Version 1.1.2

- the method `prepareSaveResource` removed the `activePermissions` property
