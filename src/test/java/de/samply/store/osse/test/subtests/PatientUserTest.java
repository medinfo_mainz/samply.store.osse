package de.samply.store.osse.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.NotAuthorizedException;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEVocabulary;

/**
 *
 */
public class PatientUserTest extends AbstractStoreTest {

    @Before
    public void setup() throws DatabaseException {
        super.setup("patientUser", "patientUser");
    }

    @Test
    public void testSelfPatient() throws DatabaseException {
        ArrayList<Resource> patients = model.getResources(OSSEVocabulary.Type.Patient);

        assertTrue(patients.size() == 1);

        ArrayList<Resource> cases = model.getResources(OSSEVocabulary.Type.Case);

        assertTrue(cases.size() == 1);

        Resource Case = cases.get(0);

        Resource caseForm = model.createResource(OSSEVocabulary.Type.CaseForm);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Case, Case);

        exception.expect(NotAuthorizedException.class);
        saveResources(caseForm);
        exception = ExpectedException.none();
    }

    @Test
    public void testCaseForm() throws DatabaseException {
        ArrayList<Resource> cases = model.getResources(OSSEVocabulary.Type.Case);
        ArrayList<Resource> statuses = model.getResources(OSSEVocabulary.Type.Status);

        Resource Case = cases.get(0);

        Resource caseForm = model.createResource(OSSEVocabulary.Type.CaseForm);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Case, Case);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Name, "Just a random caseform name!");
        caseForm.setProperty(OSSEVocabulary.CaseForm.Version, 1);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Status, statuses.get(0));
        caseForm.setProperty(OSSEOntology.FormType.FormType, OSSEOntology.FormType.PATIENTFORM);

        saveResources(caseForm);
    }

}
