/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.osse.query;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import de.samply.share.model.osse.And;
import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.ConditionType;
import de.samply.share.model.osse.Eq;
import de.samply.share.model.osse.Geq;
import de.samply.share.model.osse.Gt;
import de.samply.share.model.osse.IsNotNull;
import de.samply.share.model.osse.IsNull;
import de.samply.share.model.osse.Leq;
import de.samply.share.model.osse.Like;
import de.samply.share.model.osse.Lt;
import de.samply.share.model.osse.Neq;
import de.samply.share.model.osse.Or;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.Where;
import de.samply.store.BooleanLiteral;
import de.samply.store.DatabaseConstants;
import de.samply.store.DatabaseDefinitions;
import de.samply.store.JSONResource;
import de.samply.store.Literal;
import de.samply.store.NumberLiteral;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.StringLiteral;
import de.samply.store.TimeLiteral;
import de.samply.store.TimestampLiteral;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.osse.exception.UnknownAttributesException;
import de.samply.store.osse.query.QueryUtil.ClauseMode;
import de.samply.store.query.ArraySelection;
import de.samply.store.query.ArrayValueColumn;
import de.samply.store.query.ArrayValueCriteria;
import de.samply.store.query.Criteria;
import de.samply.store.query.GreaterCriteria;
import de.samply.store.query.GreaterOrEqualCriteria;
import de.samply.store.query.LessCriteria;
import de.samply.store.query.LessOrEqualCriteria;
import de.samply.store.query.ListCriteria;
import de.samply.store.query.OrCriteria;
import de.samply.store.query.PropertyCriteria;
import de.samply.store.query.ResourceQuery;
import de.samply.store.query.Selection;
import de.samply.store.query.ValueCriteria;
import de.samply.store.sql.Clauses;
import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLColumnType;
import de.samply.store.sql.SQLQuery;
import de.samply.store.sql.SQLTemporaryTable;

/**
 * Converts a View from the query language into a resource query that can be parsed by the PSQLModel.
 * This builder requires the Samply.EDC.OSSE configuration.
 */
public class ResourceQueryBuilder {

    /**
     * Some static strings necessary to access the 'osse' configuration from EDC.
     */
    private static final String RECORD_URN = "recordUrn";
    private static final String TYPE = "type";
    private static final String ENUM_DATE_FORMAT = "enumDateFormat";
    private static final String EPISODE = "EPISODE";
    private static final String PERMISSIBLE_VALUES = "permissibleValues";
    private static final String CASE = "CASE";
    private static final String FIELD = "FIELD";
    private static final String FORM_NAME = "formName";
    private static final String FIELD_TYPE = "fieldType";
    private static final String MDR_ENTITY_IN_FORM = "mdrEntityIsInForm";
    private static final String FORM_TYPE = "formType";
    private static final String RECORD_HAS_ENTRIES = "recordHasMdrEntities";
    private static final String VALIDATION = "mdrEntityHasValidation";

    private Set<String> keys = new HashSet<>();

    private final View view;

    /**
     * Key: URN, Value: list of forms that use the dataelement.
     */
    private HashMap<String, List<Value>> entities = new HashMap<>();

    /**
     * Key: URN, value: validation information for this dataelement.
     */
    private HashMap<String, JSONResource> validation = new HashMap<>();

    private final Criteria finalCriteria;

    /**
     * The current query configuration
     */
    private final QueryConfig qConfig;

    /**
     * Key: URN without version
     * Value: List of dataelements with the same URN (except the version)
     */
    private HashMap<String, List<String>> dataelements = new HashMap<>();

    /**
     * Current list of selections.
     */
    private List<Selection> selection = new ArrayList<>();

    /**
     * The set of selected properties, the keys are URNs
     */
    private HashSet<String> selectedProperties = new HashSet<>();

    /**
     * The selection map with the URN as key and a hashmap of JSONResources and the selection as value.
     */
    private HashMap<String, HashMap<JSONResource, Selection>> selectionMap = new HashMap<>();

    /**
     * The maps used for the raw results.
     */
    HashMap<String, HashMap<Integer, Resource>> resourceMap = new HashMap<>();

    /**
     * The date values for special MDR keys
     */
    private Date minChangedDate = null;
    private Date maxChangedDate = null;
    private Date minEpisodeDate= null;
    private Date maxEpisodeDate = null;
    private Date minRegisteredDate = null;
    private Date maxRegisteredDate = null;

    private final SimpleDateFormat specialDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     *
     * @param view
     * @param config
     * @throws UnknownAttributesException
     * @throws ParseException
     * @throws DatabaseException
     */
    public ResourceQueryBuilder(View view, JSONResource config, QueryConfig qConfig) throws UnknownAttributesException, ParseException, DatabaseException {
        this.view = view;
        this.qConfig = qConfig;

        JSONResource keys = config.getProperty(MDR_ENTITY_IN_FORM).asJSONResource();
        for(String key : keys.getDefinedProperties()) {
            entities.put(key, keys.getProperties(key));
        }

        keys = config.getProperty(RECORD_HAS_ENTRIES).asJSONResource();
        for(String key : keys.getDefinedProperties()) {
            for(Value element : keys.getProperties(key)) {
                String dataKey = element.getValue();

                if(!entities.containsKey(dataKey)) {
                    entities.put(dataKey, new ArrayList<Value>());
                }

                for(Value recordEntity : entities.get(key)) {
                    JSONResource record = recordEntity.asJSONResource();
                    record.setProperty(RECORD_URN, key);
                    entities.get(dataKey).add(record);
                }
            }
        }

        for(Entry<String, List<Value>> entry : entities.entrySet()) {
            String withoutVersion = removeVersion(entry.getKey());

            if(!dataelements.containsKey(withoutVersion)) {
                dataelements.put(withoutVersion, new ArrayList<String>());
            }

            dataelements.get(withoutVersion).add(entry.getKey());
        }

        keys = config.getProperty(VALIDATION).asJSONResource();
        for(String key : keys.getDefinedProperties()) {
            for(Value element : keys.getProperties(key)) {
                validation.put(key, element.asJSONResource());
            }
        }

        Criteria criteria = null;

        /**
         * The first step is to convert the object from the query language into
         * a criteria that can be parsed more easily.
         * Converts
         *
         * <Eq>
         *   <Attribute>urn:osse-1:dataelement:1:2</Attribute>
         *   <Value>5</Value>
         * </Eq>
         *
         * To Criteria.Equal('urn:osse-1:dataelement:1:2', 'urn:osse-1:dataelement:1:2',
         *        new NumberLiteral(5))
         *
         * The type of the literal depends on the validation data of the dataelement.
         */
        if(view.getQuery() != null) {
            Where where = view.getQuery().getWhere();
            if(where != null && where.getAndOrEqOrLike().size() > 0) {
                Object obj = where.getAndOrEqOrLike().get(0);

                criteria = convertToCriteria(obj);

                /**
                 * If this is an empty list, ignore it.
                 */
                if(criteria instanceof ListCriteria) {
                    if(((ListCriteria) criteria).getAll().size() == 0) {
                        criteria = null;
                    }
                }
            }
        }

        /**
         * Check at the end of the coversion, if there are any unknown elements.
         */
        Set<String> unknownAttributes = getUnknownAttributes();

        /**
         * If there are some unknown attributes, abort with an error message.
         */
        if(unknownAttributes.size() > 0) {
            throw new UnknownAttributesException(unknownAttributes);
        }

        if(view.getViewFields() != null && !qConfig.isIgnoreViewfield()) {
            for(String key : view.getViewFields().getMdrKey()) {
                if(qConfig.isIgnoreViewfieldRevision()) {
                    throw new UnsupportedOperationException("Unsupported: can not ignore viewfield revision");
                } else {
                    for(Value v : entities.get(key)) {
                        if(isRecordOrList((JSONResource) v) || !selectedProperties.contains(key)) {
                            selection.add(convertSelection(key, v));
                            selectedProperties.add(key);
                        }
                    }
                }
            }
        }

        /**
         * The second step is to convert the criterias into criterias that are suited to the
         * currently available forms. Especially if the forms change, e.g. dataelement x is not repeatable in
         * version 1, but is in a repeatable record in version 2, this step is crucial.
         */
        if(criteria != null) {
            finalCriteria = convertCriteria(criteria);
        } else {
            finalCriteria = null;
        }
    }

    /**
     * Executes the
     * @return
     * @throws DatabaseException
     * @throws SQLException
     *
     */
   public List<Resource> execute(PSQLModel<?> model) throws DatabaseException {
       ResourceQuery resourceQuery = getResourceQuery(OSSEVocabulary.Type.Patient);

       if(qConfig.isIgnoreViewfield()) {
           return model.getResources(resourceQuery);
       }

       SQLQuery sqlQuery = resourceQuery.prepareSQLQuery();

       handleSpecialFilters(sqlQuery);

       /**
        * Add the episode timestamp selection, but only if the episode is available
        */
       SQLColumn timestampColumn = null;

       if(sqlQuery.findTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Episode).table) != null) {
           timestampColumn = sqlQuery.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Episode).table)
               .getColumn(DatabaseConstants.dataColumn, SQLColumnType.JSON_TIMESTAMP, QueryUtil.TIMESTAMP);
           sqlQuery.addSelection(timestampColumn);
       }

       /**
        * Execute the query.
        */
       try(PreparedStatement stmt = model.executeRaw(sqlQuery); ResultSet set = stmt.executeQuery()) {

           SQLColumn patientIdCol = null, caseIdCol = null, episodeIdCol = null, caseFormIdCol = null, episodeFormIdCol = null;

           Set<SQLColumn> keySet = sqlQuery.getSelection().keySet();

           /**
            * Get the columns for the IDs of: patients, cases, case forms, episodes and episode forms.
            */
           for(SQLColumn col : keySet) {
               if(!(col.table instanceof SQLTemporaryTable) && col.column.equals(DatabaseConstants.idColumn)) {
                   String type = DatabaseDefinitions.getType(col.table.getTable());

                   switch(type) {
                       case OSSEVocabulary.Type.Patient:
                           patientIdCol = col;
                           break;

                       case OSSEVocabulary.Type.Case:
                           caseIdCol = col;
                           break;

                       case OSSEVocabulary.Type.Episode:
                           episodeIdCol = col;
                           break;

                       case OSSEVocabulary.Type.CaseForm:
                           caseFormIdCol = col;
                           break;

                       case OSSEVocabulary.Type.EpisodeForm:
                           episodeFormIdCol = col;
                           break;

                       default:
                           throw new UnsupportedOperationException();
                   }
               }
           }

           List<Resource> target = new ArrayList<>();

           HashMap<SQLColumn, HashSet<Integer>> processedRowsMap = new HashMap<>();

           while(set.next()) {
               HashMap<String, JSONResource> recordMap = new HashMap<>();

               int patientId = set.getInt(patientIdCol.alias);

               int caseFormId = 0, caseId = 0;;
               int episodeId = 0, episodeFormId = 0;

               /**
                * Get the IDs.
                */
               if(caseIdCol != null) {
                   caseId = set.getInt(caseIdCol.alias);
               }

               if(caseFormIdCol != null) {
                   caseFormId = set.getInt(caseFormIdCol.alias);
               }

               if(episodeIdCol != null) {
                   episodeId = set.getInt(episodeIdCol.alias);
               }

               if(episodeFormIdCol != null) {
                   episodeFormId = set.getInt(episodeFormIdCol.alias);
               }

               Resource patient = getResource(patientId, OSSEVocabulary.Type.Patient);
               Resource Case = null, caseForm = null;
               Resource episode = null, episodeForm = null;

               /**
                * Get the resources if possible
                */
               if(caseId != 0) {
                   Case = getResource(caseId, OSSEVocabulary.Type.Case);

                   if(!patient.getProperties(OSSEVocabulary.Patient.ReadOnly.Cases).contains(Case)) {
                       patient.addProperty(OSSEVocabulary.Patient.ReadOnly.Cases, Case);
                   }
               }

               if(caseFormId != 0) {
                   caseForm = getResource(caseFormId, OSSEVocabulary.Type.CaseForm);

                   if(!Case.getProperties(OSSEVocabulary.Case.ReadOnly.CaseForms).contains(caseForm)) {
                       Case.addProperty(OSSEVocabulary.Case.ReadOnly.CaseForms, caseForm);
                   }
               }

               if(episodeId != 0) {
                   episode = getResource(episodeId, OSSEVocabulary.Type.Episode);

                   /**
                    * Set the timestamp if it is available and hasn't been added yet.
                    */
                   if(! episode.getDefinedProperties().contains(QueryUtil.TIMESTAMP)) {
                       Timestamp timestamp = set.getTimestamp(timestampColumn.alias);

                       if(timestamp != null) {
                           episode.addProperty(QueryUtil.TIMESTAMP, new TimestampLiteral(timestamp));
                       }
                   }

                   if(!Case.getProperties(OSSEVocabulary.Case.ReadOnly.Episodes).contains(episode)) {
                       Case.addProperty(OSSEVocabulary.Case.ReadOnly.Episodes, episode);
                   }
               }

               if(episodeFormId != 0) {
                   episodeForm = getResource(episodeFormId, OSSEVocabulary.Type.EpisodeForm);

                   if(!episode.getProperties(OSSEVocabulary.Episode.ReadOnly.EpisodeForms).contains(episodeForm)) {
                       episode.addProperty(OSSEVocabulary.Episode.ReadOnly.EpisodeForms, episodeForm);
                   }
               }

               /**
                * For every URN
                */
               for(Entry<String, HashMap<JSONResource, Selection>> entry : selectionMap.entrySet()) {
                   String urn = entry.getKey();

                   /**
                    * Get the selection and put the value into the appropriate resource/record
                    */
                   for(Entry<JSONResource, Selection> secEntry : entry.getValue().entrySet()) {
                       JSONResource entity = secEntry.getKey();
                       Selection selection = secEntry.getValue();

                       SQLColumn col = resourceQuery.getSelectionMap().get(selection);

                       Resource targetForm = null;

                       if(entity.getProperty(FORM_TYPE).getValue().equals("CASE")) {
                           targetForm = caseForm;
                       } else {
                           targetForm = episodeForm;
                       }

                       if(col != null && targetForm != null) {
                           targetForm.setProperty(OSSEVocabulary.CaseForm.Name, entity.getProperty(FORM_NAME).getValue());
                           Object object = set.getObject(col.alias);

                           if(object != null) {
                               if(!isRecordOrList(entity)) {
                                   /**
                                    * If it is just one property, just set the property and be done with it.
                                    */
                                   targetForm.setProperty(urn, toValue(object));
                               } else {
                                   /**
                                    * At this point selection must be an ArraySelection
                                    */
                                   if(! (selection instanceof ArraySelection)) {
                                       throw new UnsupportedOperationException();
                                   }

                                   SQLColumn rnumCol = resourceQuery.getArraySelectionMap().get(col);
                                   int rnum = set.getInt(rnumCol.alias);

                                   /**
                                    * Check, if this row hasn't been processed yet.
                                    */
                                   if(processRow(processedRowsMap, col, rnum)) {
                                       if(entity.getProperty(FIELD_TYPE).getValue().equals("REPEATABLEFIELD")) {
                                           targetForm.addProperty(urn, toValue(object));
                                       } else {
                                           String recordUrn = entity.getProperty(RECORD_URN).getValue();

                                           if(! recordMap.containsKey(recordUrn)) {
                                               recordMap.put(recordUrn, new JSONResource());
                                               targetForm.addProperty(recordUrn, recordMap.get(recordUrn));
                                           }

                                           recordMap.get(recordUrn).setProperty(urn, toValue(object));
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
           }

           if(resourceMap.size() > 0) {
               for(Entry<Integer, Resource> p : resourceMap.get(OSSEVocabulary.Type.Patient).entrySet()) {
                   target.add(p.getValue());
               }
           }

           return target;
       } catch(SQLException e) {
           throw new DatabaseException(e);
       }
   }

   /**
    *  This method handles the special filters and adds all necessary SQL Clauses.
     * @param sqlQuery
     */
    private void handleSpecialFilters(SQLQuery sqlQuery) {
        SQLColumn patientId = sqlQuery.getMainTable().getColumn(DatabaseConstants.idColumn);

        if(minChangedDate != null) {
            SQLQuery query = QueryUtil.createLastChangedQuery(minChangedDate, ClauseMode.GREATER);
            sqlQuery.addClause(Clauses.In(patientId, query));
        }

        if(maxChangedDate != null) {
            SQLQuery query = QueryUtil.createLastChangedQuery(maxChangedDate, ClauseMode.LESS);
            sqlQuery.addClause(Clauses.In(patientId, query));
        }

        if(minRegisteredDate != null) {
            SQLQuery query = QueryUtil.createRegisteredQuery(minRegisteredDate, ClauseMode.GREATER);
            sqlQuery.addClause(Clauses.In(patientId, query));
        }

        if(maxRegisteredDate != null) {
            SQLQuery query = QueryUtil.createRegisteredQuery(maxRegisteredDate, ClauseMode.LESS);
            sqlQuery.addClause(Clauses.In(patientId, query));
        }

        /**
         * If the episode tables hasn't been used, then ignore it.
         */
        if(sqlQuery.findTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Episode).table) != null) {
            SQLColumn episodeId = sqlQuery.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Episode).table)
                    .getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);

            if(minEpisodeDate != null) {
                SQLQuery query = QueryUtil.createEpisodeQuery(minEpisodeDate, ClauseMode.GREATER);
                sqlQuery.addClause(Clauses.In(episodeId, query));
            }

            if(maxEpisodeDate != null) {
                SQLQuery query = QueryUtil.createEpisodeQuery(maxEpisodeDate, ClauseMode.LESS);
                sqlQuery.addClause(Clauses.In(episodeId, query));
            }
        }
    }

    /**
     * Returns the appropriate Samply Store value.
     *
     * @param object
     * @return
     */
    private Value toValue(Object object) {
        if (object instanceof Timestamp) {
            return new TimestampLiteral((Timestamp) object);
        } else if (object instanceof Time) {
            return new TimeLiteral((Time) object);
        } else if (object instanceof Number) {
            return new NumberLiteral((Number) object);
        } else {
            return new StringLiteral(object.toString());
        }
    }

    /**
     * if true, the given row has already been processed for the given column and row number.
     * @param processedRowsMap
     * @param col
     * @param rnum
     * @return
     */
    private boolean processRow(HashMap<SQLColumn, HashSet<Integer>> processedRowsMap,
            SQLColumn col, int rnum) {
        if(!processedRowsMap.containsKey(col)) {
            processedRowsMap.put(col, new HashSet<Integer>());
        }

        if(processedRowsMap.get(col).contains(rnum)) {
            return false;
        } else {
            processedRowsMap.get(col).add(rnum);
            return true;
        }
    }

    /**
     * Returns the resource with the given type and ID.
     * @param id
     * @param table
     */
    private Resource getResource(Integer id, String type) {
        if(!resourceMap.containsKey(type)) {
            resourceMap.put(type, new HashMap<Integer, Resource>());
        }

        if(!resourceMap.get(type).containsKey(id)) {
            resourceMap.get(type).put(id, new Resource(type, id));
        }
        return resourceMap.get(type).get(id);
    }

    /**
     * Returns the resource query for the view.
     * @param type
     * @return
     */
    public ResourceQuery getResourceQuery(String type) {
        ResourceQuery query = new ResourceQuery(type, !qConfig.isIgnoreViewfield());
        if(finalCriteria != null) {
            query.add(finalCriteria);
        }

        for(Selection sel : selection) {
            query.addSelection(sel);
        }
        return query;
    }

    /**
     * Returns the unknown attributes for this query.
     * @return
     * @throws ParseException
     * @throws InvalidOperationException
     */
    private Set<String> getUnknownAttributes() throws ParseException {
        Set<String> unknownAttributes = new HashSet<>();

        if(view.getViewFields() != null) {
            for(String key : view.getViewFields().getMdrKey()) {
                if(!validation.containsKey(key)) {
                    unknownAttributes.add(key);
                }
            }
        }

        for(String key : keys) {
            if(!validation.containsKey(key) && !QueryUtil.SPECIAL_KEYS.contains(key)) {
                unknownAttributes.add(key);
            }
        }

        return unknownAttributes;
    }

    public Criteria convertCriteria() throws ParseException {
        return finalCriteria;
    }

    /**
     * This method converts a Criteria into one or more Criterias that the ResourceQuery can handle.
     * It depends on the structure of the forms how this conversion is executed.
     * @param criteria
     * @return
     * @throws ParseException
     */
    private Criteria convertCriteria(Criteria criteria) throws ParseException {
        if(criteria instanceof ValueCriteria || criteria instanceof PropertyCriteria) {
            PropertyCriteria pc = (PropertyCriteria) criteria;

            if(qConfig.isIgnoreQueryRevision()) {
                OrCriteria or = Criteria.Or();

                for(String urn : dataelements.get(removeVersion(pc.property))) {
                    for(Value v : entities.get(urn)) {
                        or.add(convertCriteria(pc, v, urn));
                    }
                }
                return or;
            } else {
                OrCriteria or = Criteria.Or();

                for(Value v : entities.get(pc.property)) {
                    or.add(convertCriteria(pc, v, pc.property));
                }
                return or;
            }
        } else if(criteria instanceof ListCriteria) {
            ListCriteria lc = (ListCriteria) criteria;
            List<Criteria> listCriteria = new ArrayList<>();

            for(Criteria c : lc.getAll()) {
                listCriteria.add(convertCriteria(c));
            }

            return lc.clone(listCriteria);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * @param key
     * @param v
     * @return
     */
    private Selection convertSelection(String urn, Value entity) {
        if(!(entity instanceof JSONResource)) {
            throw new UnsupportedOperationException("Unknown entity: " + entity.getClass().getCanonicalName());
        }

        JSONResource e = (JSONResource) entity;
        String type = getFormType(e);

        Selection target = null;
        if(isRecordOrList(e)) {
            /**
             * If this value is either a record, a reapeatable record, or a repeatable field, use the array value column
             * because the value needs to be extracted from the JSON array in '{urn,column}'.
             */
            String recordUrn = null;

            if(e.getProperty(RECORD_URN) != null) {
                recordUrn = e.getProperty(RECORD_URN).getValue();
            } else {
                recordUrn = urn;
            }

            target = new ArraySelection(type, recordUrn, e.getProperty(FORM_NAME).getValue(),
                    getSQLColumnType(urn), urn);
        } else {
            target = new Selection(type, getSQLColumnType(urn), urn);
        }

        if(!selectionMap.containsKey(urn)) {
            selectionMap.put(urn, new HashMap<JSONResource, Selection>());
        }

        selectionMap.get(urn).put(e, target);

        return target;
    }

    private boolean isRecordOrList(JSONResource e) {
        return e.getProperty(RECORD_URN) != null || !e.getProperty(FIELD_TYPE).getValue().equals(FIELD);
    }

    /**
     * Converts the given criteria into a criteria that respects the current form configuration.
     * @param pc
     * @param list
     * @return
     * @throws ParseException
     */
    private Criteria convertCriteria(PropertyCriteria pc, Value entity, String urn) throws ParseException {
        if(!(entity instanceof JSONResource)) {
            throw new UnsupportedOperationException("Unknown entity: " + entity.getClass().getCanonicalName());
        }

        JSONResource e = (JSONResource) entity;
        String type = getFormType(e);

        if(isRecordOrList(e)) {
            /**
             * If this value is either a record, a reapeatable record, or a repeatable field, use the array value column
             * because the value needs to be extracted from the JSON array in '{urn,column}'.
             */
            String recordUrn = null;

            if(e.getProperty(RECORD_URN) != null) {
                recordUrn = e.getProperty(RECORD_URN).getValue();
            } else {
                recordUrn = urn;
            }

            ArrayValueColumn column = new ArrayValueColumn(type, recordUrn, e.getString(FORM_NAME),
                    getSQLColumnType(pc.property), pc.property);
            if(pc instanceof ValueCriteria) {
                ValueCriteria vc = (ValueCriteria) pc;
                return new ArrayValueCriteria(column, vc.clone(type, urn, vc.value));
            } else {
                return new ArrayValueCriteria(column, pc.clone(type, urn));
            }
        } else {
            if(pc instanceof ValueCriteria) {
                ValueCriteria vc = (ValueCriteria) pc;
                return vc.clone(type, urn, vc.value);
            } else {
                return pc.clone(type, urn);
            }
        }
    }

    /**
     * Returns the form type for the given entity.
     * @param entity
     * @return
     */
    private static String getFormType(JSONResource entity) {
        switch(entity.getProperty(FORM_TYPE).getValue()) {
            case CASE:
                return OSSEVocabulary.Type.CaseForm;

            case EPISODE:
                return OSSEVocabulary.Type.EpisodeForm;

            default:
                throw new UnsupportedOperationException("Unknown formType: " + entity.getProperty(FORM_TYPE).getValue());
        }
    }

    /**
     * Returns the Literal for the given value and URN key.
     * @param value
     * @param mdrKey
     * @return
     * @throws ParseException
     */
    private Literal<?> convertValue(String value, String mdrKey) throws ParseException {
        JSONResource v = validation.get(mdrKey);

        /**
         * If the key is one of the special date formats, convert it into a timestamp literal.
         */
        if(QueryUtil.isSpecialDate(mdrKey)) {
            return new TimestampLiteral(specialDateFormat.parse(value).getTime());
        }

        if(v == null) {
            /**
             * this is an unknown property, ignore for now. After we have all
             * the unknown properties, an exception is thrown.
             */
            return null;
        }

        String type = v.getProperty(TYPE).getValue();

        if(type.equals(PERMISSIBLE_VALUES)) {
            return new StringLiteral(value);
        }

        switch(EnumValidationType.valueOf(type)) {
            case INTEGER:
            case INTEGERRANGE:
                return new NumberLiteral(Integer.valueOf(value));

            case FLOAT:
            case FLOATRANGE:
                return new NumberLiteral(Float.valueOf(value));

            case NONE:
            case REGEX:
                return new StringLiteral(value);

            case BOOLEAN:
                return new BooleanLiteral(Boolean.valueOf(value));

            case DATE:
            case DATETIME:
                return new TimestampLiteral(getDate(v, value));

            case TIME:
                return new TimeLiteral(getTime(v, value));

            default:
                throw new UnsupportedOperationException("Unsupported value: " + type);
        }
    }

    /**
     * Returns the SQLColumn type for the given MDR Key.
     * @param mdrKey
     * @return
     */
    private SQLColumnType getSQLColumnType(String mdrKey) {
        JSONResource v = validation.get(mdrKey);
        String type = v.getProperty(TYPE).getValue();

        if(type.equals(PERMISSIBLE_VALUES)) {
            return SQLColumnType.JSON_STRING;
        }

        switch(EnumValidationType.valueOf(type)) {
            case INTEGER:
            case INTEGERRANGE:
                return SQLColumnType.JSON_INT;

            case FLOAT:
            case FLOATRANGE:
                return SQLColumnType.JSON_FLOAT;

            case NONE:
            case REGEX:
                return SQLColumnType.JSON_STRING;

            case BOOLEAN:
                return SQLColumnType.JSON_BOOLEAN;

            case DATE:
            case DATETIME:
                return SQLColumnType.JSON_TIMESTAMP;

            case TIME:
                return SQLColumnType.JSON_TIME;

            default:
                throw new UnsupportedOperationException("Unsupported value: " + type);
        }
    }


    /**
     * @param v
     * @param value
     * @return
     * @throws ParseException
     */
    private long getDate(JSONResource v, String value) throws ParseException {
        SimpleDateFormat format;
        String enumFormat = v.getProperty(ENUM_DATE_FORMAT).getValue();

        switch(enumFormat) {
            case "DIN_5008_WITH_DAYS":
                format = new SimpleDateFormat("dd.MM.yyyy");
                break;

            case "DIN_5008":
                format = new SimpleDateFormat("MM.yyyy");
                break;

            case "ISO_8601":
                format = new SimpleDateFormat("yyyy-MM");
                break;

            case "ISO_8601_WITH_DAYS":
                format = new SimpleDateFormat("yyyy-MM-dd");
                break;

            default:
                throw new UnsupportedOperationException("Unknown date format: " + enumFormat);
        }

        return format.parse(value).getTime();
    }

    /**
     * @param v
     * @param value
     * @return
     * @throws ParseException
     */
    private Time getTime(JSONResource v, String value) throws ParseException {
        SimpleDateFormat format;
        String enumFormat = v.getProperty(ENUM_DATE_FORMAT).getValue();

        switch(enumFormat) {
            case "HOURS_24":
                format = new SimpleDateFormat("HH:mm");
                break;

            case "HOURS_24_WITH_SECONDS":
                format = new SimpleDateFormat("HH:mm:ss");
                break;

            case "HOURS_12":
                format = new SimpleDateFormat("hh:mm aa");
                break;

            case "ISO_8601_WITH_DAYS":
                format = new SimpleDateFormat("hh:mm:ss aa");
                break;

            default:
                throw new UnsupportedOperationException("Unknown date format: " + enumFormat);
        }

        return new Time(format.parse(value).getTime());
    }

    /**
     * Converts a object from the query language into a criteria.
     * @param c
     * @return
     * @throws ParseException
     */
    public Criteria convertToCriteria(Object c) throws ParseException {
        if (c instanceof Eq) {
            Attribute attribute = ((Eq) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.Equal(mdrKey, mdrKey, convertValue(value, mdrKey));
        } else if (c instanceof Geq) {
            Attribute attribute = ((Geq) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.GreaterOrEqual(mdrKey, mdrKey, convertValue(value, mdrKey));
        } else if (c instanceof Gt) {
            Attribute attribute = ((Gt) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.Greater(mdrKey, mdrKey, convertValue(value, mdrKey));
        } else if (c instanceof Leq) {
            Attribute attribute = ((Leq) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.LessOrEqual(mdrKey, mdrKey, convertValue(value, mdrKey));
        } else if (c instanceof Like) {
            Attribute attribute = ((Like) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.SimilarTo(mdrKey, mdrKey, value);
        } else if (c instanceof Lt) {
            Attribute attribute = ((Lt) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.Less(mdrKey, mdrKey, convertValue(value, mdrKey));
        } else if (c instanceof Neq) {
            Attribute attribute = ((Neq) c).getAttribute();
            String mdrKey = attribute.getMdrKey().toString();
            String value = attribute.getValue().getValue();
            keys.add(mdrKey);
            return Criteria.NotEqual(mdrKey, mdrKey, convertValue(value, mdrKey));
        } else if (c instanceof IsNull) {
            String mdrKey = ((IsNull) c).getMdrKey().toString();
            keys.add(mdrKey);
            return Criteria.IsNull(mdrKey, mdrKey);
        } else if (c instanceof IsNotNull) {
            String mdrKey = ((IsNotNull) c).getMdrKey().toString();
            keys.add(mdrKey);
            return Criteria.IsNotNull(mdrKey, mdrKey);
        } else if (c instanceof ConditionType) {
            List<Criteria> criterias = new ArrayList<>();

            for (Object obj : ((ConditionType) c).getAndOrEqOrLike()) {
                Criteria convertToCriteria = convertToCriteria(obj);

                if(! isSpecialKey(convertToCriteria)) {
                    if(convertToCriteria instanceof ListCriteria) {
                        /**
                         * if the list is empty, ignore it.
                         */
                        if(((ListCriteria) convertToCriteria).getAll().size() > 0) {
                            criterias.add(convertToCriteria);
                        }
                    } else {
                        criterias.add(convertToCriteria);
                    }
                }
            }

            if (c instanceof And) {
                return Criteria.And().add(criterias);
            }

            if (c instanceof Or) {
                return Criteria.Or().add(criterias);
            }
        }

        return null;
    }

    /**
     * @param convertToCriteria
     * @return
     */
    private boolean isSpecialKey(Criteria criteria) {
        if(criteria instanceof ValueCriteria) {
            ValueCriteria propCriteria = (ValueCriteria) criteria;

            if(QueryUtil.SPECIAL_KEYS.contains(propCriteria.property)) {
                String key = propCriteria.property;
                Date date = propCriteria.value.asTimestamp();

                if(QueryUtil.MDRKEY_CHANGED.equals(key)
                        && (criteria instanceof LessCriteria || criteria instanceof LessOrEqualCriteria)) {
                    maxChangedDate = date;
                }

                if(QueryUtil.MDRKEY_CHANGED.equals(key)
                        && (criteria instanceof GreaterCriteria || criteria instanceof GreaterOrEqualCriteria)) {
                    minChangedDate = date;
                }

                if(QueryUtil.MDRKEY_EPISODE.equals(key)
                        && (criteria instanceof LessCriteria || criteria instanceof LessOrEqualCriteria)) {
                    maxEpisodeDate = date;
                }

                if(QueryUtil.MDRKEY_EPISODE.equals(key)
                        && (criteria instanceof GreaterCriteria || criteria instanceof GreaterOrEqualCriteria)) {
                    minEpisodeDate = date;
                }

                if(QueryUtil.MDRKEY_REGISTERED.equals(key)
                        && (criteria instanceof LessCriteria || criteria instanceof LessOrEqualCriteria)) {
                    maxRegisteredDate = date;
                }

                if(QueryUtil.MDRKEY_REGISTERED.equals(key)
                        && (criteria instanceof GreaterCriteria || criteria instanceof GreaterOrEqualCriteria)) {
                    minRegisteredDate = date;
                }

                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * Removes the revision from the given urn.
     * @param urn
     * @return
     */
    private String removeVersion(String urn) {
        return urn.replaceFirst("(urn:.*:.*:.*):(.+?)$", "$1");
    }

}
