package de.samply.store.osse.query;

/**
 *
 */
public class QueryConfig {

    /**
     * If true, the resource query builder will ignore the revision of a dataelement for
     * the query part of the view.
     */
    private boolean ignoreQueryRevision = false;

    /**
     * If true, the resource query builder will ignore the revision of a datalement
     * for the viewfields part of the view.
     */
    private boolean ignoreViewfieldRevision = false;

    /**
     * If true, the resource query will ignore the view fields altogether.
     */
    private boolean ignoreViewfield = false;

    /**
     * @return the ignoreQueryRevision
     */
    public boolean isIgnoreQueryRevision() {
        return ignoreQueryRevision;
    }

    /**
     * @param ignoreQueryRevision the ignoreQueryRevision to set
     */
    public void setIgnoreQueryRevision(boolean ignoreQueryRevision) {
        this.ignoreQueryRevision = ignoreQueryRevision;
    }

    /**
     * @return the ignoreViewfieldRevision
     */
    public boolean isIgnoreViewfieldRevision() {
        return ignoreViewfieldRevision;
    }

    /**
     * @param ignoreViewfieldRevision the ignoreViewfieldRevision to set
     */
    public void setIgnoreViewfieldRevision(boolean ignoreViewfieldRevision) {
        this.ignoreViewfieldRevision = ignoreViewfieldRevision;
    }

    /**
     * @return the ignoreViewfield
     */
    public boolean isIgnoreViewfield() {
        return ignoreViewfield;
    }

    /**
     * @param ignoreViewfield the ignoreViewfield to set
     */
    public void setIgnoreViewfield(boolean ignoreViewfield) {
        this.ignoreViewfield = ignoreViewfield;
    }

}
