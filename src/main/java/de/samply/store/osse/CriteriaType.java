package de.samply.store.osse;

/**
 * The criteria type: Either EQUAL (checks for equality) or REGEX (uses a regular expression)
 */
public enum CriteriaType {
    EQUAL("equal"), REGEX("regex"), UNKNOWN("unknown");

    public final String name;

    private CriteriaType(String name) {
        this.name = name;
    }
}