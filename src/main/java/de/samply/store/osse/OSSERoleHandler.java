/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.osse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.store.AccessContext;
import de.samply.store.DatabaseConstants;
import de.samply.store.Identifiable;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.TableDefinition.Join.JoinType;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.osse.OSSEData.PermissionSelection;
import de.samply.store.osse.OSSEPermission.AccessType;
import de.samply.store.query.AndCriteria;
import de.samply.store.query.Criteria;
import de.samply.store.query.OrCriteria;
import de.samply.store.query.ResourceQuery;
import de.samply.store.query.ResourceQuery.Path;
import de.samply.store.query.ResourceQuery.PathEntry;
import de.samply.store.query.ResourceQuery.Relation;
import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLColumnType;
import de.samply.store.sql.SQLIntValue;
import de.samply.store.sql.SQLQuery;
import de.samply.store.sql.SQLTemporaryTable;
import de.samply.store.sql.clauses.Clause;
import de.samply.store.sql.clauses.EqualClause;
import de.samply.store.sql.clauses.IsNotNullClause;
import de.samply.store.sql.clauses.OrClause;

/**
 * This abstract class handles the permissions for a specific role type.
 * Currently there are four different role types:
 *
 * <pre>
 * - Administrator
 * - Local Administrator
 * - Custom User
 * - Developer
 * </pre>
 *
 *
 */
public abstract class OSSERoleHandler {

    static protected final Logger logger = LogManager.getLogger(OSSERoleHandler.class);

    /**
     * The role and location resource for this role handler
     */
    protected final Resource role, location;

    /**
     * The role type of this role handler
     */
    protected final OSSERoleType roleType;

    /**
     * The access controller
     */
    protected final OSSEAccessController controller;

    /**
     * The map of resource types to a list of OSSEPermission objects
     */
    private HashMap<String, ArrayList<OSSEPermission> > permissions = new HashMap<>();

    /**
     * The same map as permissions, except the keys are the OSSEPermissions IDs.
     */
    private HashMap<Integer, OSSEPermission> permissionsById = new HashMap<>();

    /**
     * Initializes this role handler for the given role with the role type, for the
     * given location and access controller. This constructor loads all active permissions
     * for the selected role and currently active user.
     *
     * @param role role resource
     * @param roleType role type
     * @param location the location for this role handler
     * @param controller the access controller
     * @throws DatabaseException
     */
    public OSSERoleHandler(Resource role, OSSERoleType roleType,
            Resource location, OSSEAccessController controller) throws DatabaseException {
        this.role = role;
        this.location = location;
        this.roleType = roleType;
        this.controller = controller;

        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Permission);
        // Get all Permissions for the selected role, and for the user role associated with
        // the current user
        query.add(Criteria.Or(
                Criteria.Equal(OSSEVocabulary.Type.Role, OSSEVocabulary.ID, role.getId()),
                Criteria.And(
                        Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID, controller.getLogin().uid),
                        Criteria.Equal(OSSEVocabulary.Type.Role, OSSEOntology.Role.IsUserRole, true))));
        List<Resource> permissions = controller.getResourcesInternal(query);

        for(Resource r : permissions) {
            logger.debug("Adding permission with id " + r.getId());
            addPermission(r);
        }
    }

    /**
     * Configures the context for a "GET" request of the specified resource type.
     *
     * @param resourceType the resource type
     * @param context
     * @throws DatabaseException
     */
    abstract public void configureGet(String resourceType, AccessContext<OSSEData> context) throws DatabaseException;

    /**
     * Returns true if the user is allowed to create the specified resource.
     * Otherwise this method returns false.
     *
     * @param resource
     * @return
     * @throws DatabaseException
     */
    abstract public boolean canCreateResource(Resource resource) throws DatabaseException;

    /**
     * Returns true if the user is allowed to edit the specified resource.
     * Otherwise this method returns false.
     *
     * @param resource
     * @return
     * @throws DatabaseException
     */
    abstract public boolean canEditResource(Resource resource) throws DatabaseException;

    /**
     * Returns true if the user is able to access the configuration object
     * with the specified name. Otherwise this method returns false.
     *
     * @param name
     * @return
     */
    abstract public boolean canGetConfig(String name);

    /**
     * Returns true if the user is able to access the configuration object
     * with the specified name. Otherwise this method returns false.
     *
     * @param name
     * @param config
     * @return
     */
    abstract public boolean canSaveConfig(String name, JSONResource config);

    /**
     * Returns true, if the user is allowed to delete the specified resource.
     *
     * @param resource
     * @return
     * @throws DatabaseException
     */
    public boolean canDeleteResource(Resource resource) throws DatabaseException {
        return false;
    }

    /**
     * Adds the given permission objects to the given ResourceQuery.
     * Uses ORs for multiple permission objects.
     *
     * @param query the resource query
     * @param permissions
     * @throws InvalidOperationException
     */
    protected void handleWritePermissions(ResourceQuery query) throws InvalidOperationException {
        List<OSSEPermission> permissions = getPermissions(query.resultType, AccessType.WRITE);
        OrCriteria ors = Criteria.Or();
        for(OSSEPermission p : permissions) {
            AndCriteria ands = Criteria.And();

            for(OSSECriteriaEntry entry: p.getCriteriaEntries()) {
                ands.add(convertCriteria(entry));
            }

            ors.add(ands);
        }

        query.add(ors);
    }

    /**
     * Handles all "read" permissions and adds all necessary data to the access context.
     *
     * @param type the resource type
     * @param context the AccessContext
     * @throws DatabaseException
     */
    protected void handleReadPermissions(String type, AccessContext<OSSEData> context) throws DatabaseException {
        List<OSSEPermission> permissionList = getPermissions(type, AccessType.READ);

        context.data = new OSSEData();

        if(permissionList.size() > 0 ) {
            SQLColumn idColumn = context.sql.getMainTable().getColumn("id");
            List<Clause> orClauses = new ArrayList<Clause>();

            for(OSSEPermission p : permissionList) {
                // For each OSSEPermission object the server
                // creates a ResourceQuery and adds all
                // given criterias, then creates a temporary table
                // and joins it with the real query.

                // That way the AccessController is able to determine which
                // OSSEPermission object was active for a particular resource.
                // Multiple permission objects may be active for one single resource

                logger.debug("Adding permission " + p.id + " to the active query");

                ResourceQuery query = new ResourceQuery(type);

                for(OSSECriteriaEntry criteria : p.getCriteriaEntries()) {
                    query.add(convertCriteria(criteria));
                }

                SQLQuery sql = query.prepareSQLQuery();
                SQLColumn id = sql.getMainTable().getColumn(DatabaseConstants.idColumn);

                PermissionSelection sqlData = new PermissionSelection();
                sqlData.permission = p;

                SQLColumn permissionColumn = sql.getMainTable().getConstant(new SQLIntValue(p.id), SQLColumnType.INTEGER);
                sql.addSelection(permissionColumn);

                SQLTemporaryTable table = new SQLTemporaryTable(sql, context.sql);
                sqlData.permissionColumn = table.getOutsideColumn(permissionColumn);

                context.sql.addSelection(sqlData.permissionColumn);
                context.sql.addJoin(table, table.getOutsideColumn(id), idColumn);

                orClauses.add(new IsNotNullClause(sqlData.permissionColumn));
                context.data.getPermissionSelection().add(sqlData);
            }

            if(orClauses.size() == 0) {
                newInvalidClause(context.sql);
            } else {
                // Just to make sure that the result from PostgreSQL contains only
                // rows that are accessible to the client
                context.sql.addClause(new OrClause(orClauses));
            }
        } else {
            newInvalidClause(context.sql);
            logger.warn("No permissions available for " + type);
        }
    }

    /**
     * Adds a new invalid clause. That means the SQL query will return an empty
     * result set.
     * @param sql
     */
    protected void newInvalidClause(SQLQuery sql) {
        sql.addClause(new EqualClause(new SQLIntValue(1), new SQLIntValue(0)));
    }

    /**
     * Checks if the resource meets an OSSECriteriaEntry
     *
     * @param resource the resource that is checked
     * @param criteria the osse criteria entry
     * @return
     * @throws DatabaseException
     */
    protected boolean resourcesMeetsCriteria(Resource resource, OSSECriteriaEntry criteria) throws DatabaseException {
        // First, check if the criteria is specified for the resource

        if(resource.getType().equals(criteria.resourceType)) {
            for(Value v : resource.getProperties(criteria.property)) {
                if(v.equals(criteria.value)) {
                    return true;
                }
            }
            return false;
        }

        // If it is not, find a path from resource.getType to criteria.resource
        // And for each value in resource.getProperties(path.property) check
        // if a ResourceQuery for that type returns the value
        // If one of those values does, return true, if none does return false

        List<Path> paths = ResourceQuery.findPath(resource.getType(), criteria.resourceType, new ArrayList<Relation>());

        if(paths.size() == 0) {
            return false;
        }

        Path path = paths.get(0);

        PathEntry firstEntry = path.getEntries().pop();

        String property = null;

        if(firstEntry.join.joinType == JoinType.N_TO_M_JOIN) {
            property = firstEntry.join.property;
        } else {
            property = firstEntry.join.inverseProperty;
        }

        for(Value v : resource.getProperties(property)) {
            if(v instanceof Identifiable) {
                Identifiable ident = (Identifiable) v;
                ResourceQuery query = new ResourceQuery(ident.getType());
                query.add(Criteria.Equal(ident.getType(), OSSEVocabulary.ID, ident.getId()));
                query.add(convertCriteria(criteria));
                List<Resource> result = getResourcesInternal(query);

                if(result.size() > 0) {
                    if(result.get(0).getId() == ident.getId()) {
                        return true;
                    }
                }

            } else {
                throw new InvalidOperationException("Not an identifiable stored in " + firstEntry.join.property);
            }
        }

        return false;
    }

    /**
     * Checks for
     *
     * @param resource
     * @param property
     * @param type
     * @return
     * @throws DatabaseException
     */
    @Deprecated
    protected List<Value> checkDiffRelations(Resource resource, String property,
            String type) throws DatabaseException {
        ResourceQuery query = new ResourceQuery(type);
        query.add(Criteria.Equal(resource.getType(), OSSEVocabulary.ID, resource.getId()));
        List<Resource> instances = controller.getResourcesInternal(query);

        List<Value> target = new ArrayList<Value>();

        for(Value value : resource.getProperties(property)) {
            int id1 = 0, id2 = 0;
            if(value instanceof Identifiable) {
                id1 = ((Identifiable) value).getId();
            }

            boolean found = false;
            for(Value value2 : instances) {
                if(value2 instanceof Identifiable) {
                    id2 = ((Identifiable) value2).getId();

                    if(id2 == id1) {
                        found = true;
                    }
                }
            }

            if(!found) {
                target.add(value);
            }
        }

        for(Value value : instances) {
            int id1 = 0, id2 = 0;
            if(value instanceof Identifiable) {
                id1 = ((Identifiable) value).getId();
            }

            boolean found = false;
            for(Value value2 : resource.getProperties(property)) {
                if(value2 instanceof Identifiable) {
                    id2 = ((Identifiable) value2).getId();

                    if(id2 == id1) {
                        found = true;
                    }
                }
            }

            if(!found) {
                target.add(value);
            }
        }

        return target;
    }

    /**
     * Returns the result for the specified resource query. <u>Does not check permissions
     * at all.</u>
     * @param query the resource query
     * @return
     * @throws DatabaseException
     */
    protected List<Resource> getResourcesInternal(ResourceQuery query) throws DatabaseException {
        return controller.getResourcesInternal(query);
    }

    /**
     * Adds the specified resource permission object in the permissions map and array.
     * @param resource
     */
    protected void addPermission(Resource resource) {
        OSSEPermission permission = new OSSEPermission(resource);
        addPermission(permission);
    }

    /**
     * Adds the specified osse permission object into the permissions map and
     * array.
     * @param permission
     */
    protected void addPermission(OSSEPermission permission) {
        for(String resource  : permission.getResources()) {
            if(!permissions.containsKey(resource)) {
                permissions.put(resource, new ArrayList<OSSEPermission>());
            }

            permissions.get(resource).add(permission);
        }

        permissionsById.put(permission.id, permission);
    }

    /**
     * Returns all available permissions for the given resource type and access type
     * @param type
     * @param accessType
     * @return
     */
    protected List<OSSEPermission> getPermissions(String type, OSSEPermission.AccessType accessType) {
        List<OSSEPermission> target = new ArrayList<OSSEPermission>();

        if(!permissions.containsKey(type)) {
            return target;
        }

        for(OSSEPermission p : permissions.get(type)) {
            if(p.accessType == accessType) {
                target.add(p);
            }
        }

        return target;
    }

    /**
     * Returns the permission object with the specified permission id
     * @param id
     * @return
     */
    public OSSEPermission getPermission(int id) {
        return permissionsById.get(id);
    }

    /**
     * Converts an OSSE Criteria entry into a de.samply.store.criteria.Criteria object
     * @param criteria
     * @return
     * @throws InvalidOperationException
     */
    protected Criteria convertCriteria(OSSECriteriaEntry criteria) throws InvalidOperationException {
        switch(criteria.type) {
        case EQUAL:
            return Criteria.Equal(criteria.resourceType, criteria.property, criteria.value);

        case REGEX:
            return Criteria.SimilarTo(criteria.resourceType, criteria.property, criteria.value.getValue());

        default:
            logger.error("Unknown criteria type: " + criteria.type.toString());
            throw new InvalidOperationException("Unknown criteria type: " + criteria.type.toString());
        }
    }

    /**
     * Returns true if the user is allowed to execute the specified action with
     * the specified parameters. The parameters depend on the action and may be null
     * for certain actions.
     * @param name the action name
     * @param parameters the parameters for this action
     * @return
     * @throws DatabaseException
     */
    public boolean canExecuteAction(String name, Object... parameters) throws DatabaseException {
        return false;
    }

}
