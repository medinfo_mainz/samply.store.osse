/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.osse.handlers;

import java.util.ArrayList;
import java.util.List;

import de.samply.store.AccessContext;
import de.samply.store.JSONResource;
import de.samply.store.NumberLiteral;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEAccessController;
import de.samply.store.osse.OSSEBlacklistProperty;
import de.samply.store.osse.OSSECriteriaEntry;
import de.samply.store.osse.OSSEData;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEPermission;
import de.samply.store.osse.OSSEPermission.AccessType;
import de.samply.store.osse.OSSERoleHandler;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEUserType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * The Role handler for the custom role type.
 *
 *
 */
public class CustomHandler extends OSSERoleHandler {

    /**
     * Initializes this handler for the given role, location and access controller.
     *
     * @param role the role resource
     * @param location the location resource
     * @param controller the access controller
     * @throws DatabaseException
     */
    public CustomHandler(Resource role, Resource location,
            OSSEAccessController controller) throws DatabaseException {
        super(role, OSSERoleType.CUSTOM, location, controller);

        OSSEPermission p = new OSSEPermission(-1, new String[] {"location"}, AccessType.READ,
                new OSSECriteriaEntry[] {new OSSECriteriaEntry("location", "id", new NumberLiteral(location.getId()), "equal")},
                new OSSEBlacklistProperty[] {});

        addPermission(p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureGet(String type, AccessContext<OSSEData> context) throws DatabaseException {

        switch(type) {

        case OSSEVocabulary.Type.User:
        case OSSEVocabulary.Type.UserContact:
        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Location:
        case OSSEVocabulary.Type.LocationContact:
        case OSSEVocabulary.Type.Pseudonym:
        case OSSEVocabulary.Type.Status:
        case OSSEVocabulary.Type.StatusChange:
        case OSSEVocabulary.Type.Context:
        case OSSEVocabulary.Type.Registry:
        case OSSEVocabulary.Type.Permission:
            return;
        }

        handleReadPermissions(type, context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreateResource(Resource resource)
            throws DatabaseException {
        switch(resource.getType()) {
        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Permission:
        case OSSEVocabulary.Type.User:
        case OSSEVocabulary.Type.UserContact:
        case OSSEVocabulary.Type.LocationContact:
        case OSSEVocabulary.Type.Location:
        case OSSEVocabulary.Type.Status:
        case OSSEVocabulary.Type.StatusChange:
        case OSSEVocabulary.Type.Context:
            return false;
        }

        List<OSSEPermission> permissions = getPermissions(resource.getType(), AccessType.CREATE);

        for(OSSEPermission p : permissions) {
            boolean valid = true;
            for(OSSECriteriaEntry entry: p.getCriteriaEntries()) {
                if(!resourcesMeetsCriteria(resource, entry)) {
                    valid = false;
                }
            }

            if(valid) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canEditResource(Resource resource) throws DatabaseException {
        ResourceQuery query;
        switch(resource.getType()) {
        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Permission:
        case OSSEVocabulary.Type.LocationContact:
        case OSSEVocabulary.Type.Location:
        case OSSEVocabulary.Type.Status:
        case OSSEVocabulary.Type.StatusChange:
        case OSSEVocabulary.Type.Pseudonym:
        case OSSEVocabulary.Type.Context:
            return false;

        case OSSEVocabulary.Type.User:
            if(resource.getId() == controller.getLogin().uid) {
                query = new ResourceQuery(OSSEVocabulary.Type.User);
                query.add(Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID, resource.getId()));
                Resource old = getResourcesInternal(query).get(0);

                resource.removeProperties(OSSEVocabulary.User.Roles);
                for(Value oldRole : old.getProperties(OSSEVocabulary.User.Roles)) {
                    resource.addProperty(OSSEVocabulary.User.Roles, oldRole);
                }
                return true;
            } else {
                return false;
            }

        case OSSEVocabulary.Type.UserContact:
            query = new ResourceQuery(OSSEVocabulary.Type.UserContact);
            query.add(Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID, controller.getLogin().uid));
            query.add(Criteria.Equal(OSSEVocabulary.Type.UserContact, OSSEVocabulary.ID, resource.getId()));
            List<Resource> contact = getResourcesInternal(query);
            return contact.size() == 1;
        }

        ArrayList<OSSEPermission> permissions = new ArrayList<>();
        for(OSSEPermission p : this.getPermissions(resource.getType(), AccessType.WRITE)) {
            if(p.accessType == AccessType.WRITE) {
                permissions.add(p);
            }
        }

        // If there aren't *any* permissions object that grant access to edit this type of resource
        // just return false
        if(permissions.size() == 0) {
            return false;
        }

        query = new ResourceQuery(resource.getType());
        query.add(Criteria.Equal(resource.getType(), OSSEVocabulary.ID, resource.getId()));
        handleWritePermissions(query);

        List<Resource> result = getResourcesInternal(query);
        if(result.size() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canGetConfig(String name) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canSaveConfig(String name, JSONResource config) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canExecuteAction(String actionName, Object... parameters) throws DatabaseException {
        if(OSSEOntology.Actions.ChangeStatus.equals(actionName)) {
            Resource form = (Resource) parameters[0];
            Resource status = (Resource) parameters[1];
            List<OSSEPermission> permissions = getPermissions(form.getType(), AccessType.EXECUTE_ACTION);


            /**
             * First we check, if there is a permission that permits the "changeStatus" action for
             * the specified form
             */
            for(OSSEPermission p : permissions) {
                if(!actionName.equals(p.getName())) {
                    continue;
                }

                ResourceQuery query = new ResourceQuery(form.getType());
                query.add(Criteria.Equal(form.getType(), OSSEVocabulary.ID, form.getId()));

                for(OSSECriteriaEntry entry : p.getCriteriaEntries()) {
                    query.add(convertCriteria(entry));
                }

                List<Resource> results = getResourcesInternal(query);
                if(results.size() == 1) {
                    /**
                     * If we have found the form, check if the second parameter is a valid form status, that means:
                     * check if a status change describing the current change exists
                     */
                    Resource formOld = results.get(0);

                    query = new ResourceQuery(OSSEVocabulary.Type.StatusChange);
                    query.add(Criteria.Equal(OSSEVocabulary.Type.StatusChange,
                            OSSEVocabulary.StatusChange.From, formOld.getProperty(OSSEVocabulary.CaseForm.Status)));
                    query.add(Criteria.Equal(OSSEVocabulary.Type.StatusChange,
                            OSSEVocabulary.StatusChange.To, status));

                    /**
                     * If we have found an acceptable status change, return true
                     */
                    if(getResourcesInternal(query).size() > 0) {
                        return true;
                    }
                }
            }

            return false;
        } else if(OSSEOntology.Actions.CreatePatientUser.equals(actionName)) {
            Resource Case = (Resource) parameters[0];

            List<OSSEPermission> permissions = getPermissions(OSSEVocabulary.Type.User, AccessType.EXECUTE_ACTION);

            /**
             * First we check, if there is a permission that permits the "changeStatus" action for
             * the specified form
             */
            for(OSSEPermission p : permissions) {
                if(!actionName.equals(p.getName())) {
                    continue;
                }

                ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Case);
                query.add(Criteria.Equal(OSSEVocabulary.Type.Case, OSSEVocabulary.ID, Case.getId()));
                query.add(Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, location.getId()));

                List<Resource> results = getResourcesInternal(query);
                if(results.size() == 1) {
                    /**
                     * If we have found the patient, he belongs to the same locations as the user and the
                     * user should be allowed to create a patient user account. Return true
                     */
                    return true;
                }
            }

            return false;
        } else if(OSSEOntology.Actions.ChangePatientUserPassword.equals(actionName) ||
                  OSSEOntology.Actions.DisablePatientUser.equals(actionName) ||
                  OSSEOntology.Actions.EnablePatientUser.equals(actionName)) {
            Resource user = (Resource) parameters[0];

            List<OSSEPermission> permissions = getPermissions(OSSEVocabulary.Type.User, AccessType.EXECUTE_ACTION);

            /**
             * First we check, if there is a permission that permits the "changePatientUserPassword" action for
             * the specified form
             */
            for(OSSEPermission p : permissions) {
                if(!OSSEOntology.Actions.CreatePatientUser.equals(p.getName())) {
                    continue;
                }

                ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.User);
                query.add(Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID, user.getId()));
                query.add(Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, location.getId()));
                query.add(Criteria.Equal(OSSEVocabulary.Type.User, OSSEOntology.User.UserType, OSSEUserType.PATIENT.toString()));

                List<Resource> results = getResourcesInternal(query);
                if(results.size() == 1) {
                    /**
                     * If we have found the patient, he belongs to the same locations as the user and the
                     * user should be allowed to create a patient user account. Return true
                     */
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }

}
