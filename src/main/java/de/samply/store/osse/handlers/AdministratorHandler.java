/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.osse.handlers;

import de.samply.store.AccessContext;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEAccessController;
import de.samply.store.osse.OSSEData;
import de.samply.store.osse.OSSERoleHandler;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;

/**
 * The role handler for administrator roles.
 *
 */
public class AdministratorHandler extends OSSERoleHandler {

    /**
     * Initializes this handler for the given role, location and access controller.
     *
     * @param role the role resource
     * @param location the location resource
     * @param controller the access controller
     * @throws DatabaseException
     */
    public AdministratorHandler(Resource role, Resource location,
            OSSEAccessController controller) throws DatabaseException {
        super(role, OSSERoleType.ADMINISTRATOR, location, controller);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureGet(String type, AccessContext<OSSEData> context) throws DatabaseException {
        switch(type) {
        case OSSEVocabulary.Type.User:
        case OSSEVocabulary.Type.UserContact:
        case OSSEVocabulary.Type.Location:
        case OSSEVocabulary.Type.LocationContact:
        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Permission:
        case OSSEVocabulary.Type.Status:
        case OSSEVocabulary.Type.StatusChange:
        case OSSEVocabulary.Type.Pseudonym:
        case OSSEVocabulary.Type.Registry:
            return;

        }

        newInvalidClause(context.sql);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreateResource(Resource resource) {
        switch(resource.getType()) {
        case OSSEVocabulary.Type.User:
        case OSSEVocabulary.Type.UserContact:
        case OSSEVocabulary.Type.Location:
        case OSSEVocabulary.Type.LocationContact:
        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Permission:
        case OSSEVocabulary.Type.Status:
        case OSSEVocabulary.Type.StatusChange:
        case OSSEVocabulary.Type.Pseudonym:
        case OSSEVocabulary.Type.Registry:
            return true;

        default:
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canEditResource(Resource resource) {
        return canCreateResource(resource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canGetConfig(String name) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canSaveConfig(String name, JSONResource config) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canDeleteResource(Resource resource)
            throws DatabaseException {
        switch(resource.getType()) {

        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Permission:
        case OSSEVocabulary.Type.Status:
        case OSSEVocabulary.Type.StatusChange:
            return true;

        default:
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canExecuteAction(String name, Object... parameters) {
        return false;
    }

}
