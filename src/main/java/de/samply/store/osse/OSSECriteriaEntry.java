/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.osse;

import de.samply.store.Value;

/**
 * A criteria entry in the permissions object. Currently there
 * are two different types of criteria entries: EQUAL and REGEX.
 * A criteria entry is mapped to a {@link de.samply.store.query.ValueCriteria}
 * object.
 *
 *
 */
public class OSSECriteriaEntry {

    /**
     * The resource type for this criteria entry.
     */
    public final String resourceType;

    /**
     * The property for this criteria entry.
     */
    public final String property;

    /**
     * The value to check for equality or the regular expression.
     */
    public final Value value;

    /**
     * The criteria type.
     */
    public final CriteriaType type;

    /**
     * Default contructor.
     * @param resourceType the resource type
     * @param property the property
     * @param value the value or regular expression
     * @param type the criteria type.
     */
    public OSSECriteriaEntry(String resourceType, String property, Value value, String type) {
        this.resourceType = resourceType;
        this.property = property;
        this.value = value;

        if(type.equals(CriteriaType.EQUAL.name)) {
            this.type = CriteriaType.EQUAL;
        } else if(type.equals(CriteriaType.REGEX.name)) {
            this.type = CriteriaType.REGEX;
        } else {
            this.type = CriteriaType.UNKNOWN;
        }
    }

}
